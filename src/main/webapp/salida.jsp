<%-- 
    Document   : salida
    Created on : Jul 10, 2021, 5:01:51 PM
    Author     : Gera
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="cl.modelo.LAcalculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  LAcalculadora  LAcalculadora=(LAcalculadora)request.getAttribute("LAcalculadora");
  DecimalFormat formato = new DecimalFormat("#,###");
  int capital = LAcalculadora.getCapitalinicial();
  int interes = LAcalculadora.getInteressimple();

%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2 class="textos"> 
            Capital: $ <%= formato.format(capital)%>
            <br>
            <br>
            Tasa de interes: <%=LAcalculadora.getTasadeinteres()%>
            <br>
            <br>
            Años:<%= LAcalculadora.getAnos()%>
            <br>
            <br>
            Interes simple: $ <%= formato.format(capital)%>
            <br>

            <a href="https://calculadoraprimeraprueba.herokuapp.com/" class="btn btn-success">Volver</a>
    </body>
</html>
