/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;

/**
 *
 * @author Gera
 */
public class LAcalculadora {
    
    private double capitalinicial;
    private double tasadeinteres;
    private int anos;
    private int isimple;

    public int getCapitalinicial() {
        return (int) capitalinicial;
    }

    public void setCapitalinicial(int capitalinicial) {
        this.capitalinicial = capitalinicial;
    }

    public int getTasadeinteres() {
        return (int) tasadeinteres;
    }

    public void setTasadeinteres(int tasadeinteres) {
        this.tasadeinteres = tasadeinteres;
    }

    public int getAnos() {
        return anos;
    }

    public void setAnos(int anos) {
        this.anos = anos;
    }

    public int getInteressimple() {
        float iSimple;
        iSimple = (float) ((this.tasadeinteres/100)* this.capitalinicial*this.anos);
        return  (int) iSimple;
    }

    public void setIsimple(int isimple) {
        this.isimple = isimple;
    }
    
    
    
}
